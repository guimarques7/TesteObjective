package com.objective.challenge.guilherme.utils;

/**
 * @Author: Guilherme Marques
 * @Date: 2018-08-06
 *
 */
public class Node {

    public int element;
    public Node leftNode;
    public Node rightNode;
    public String value;

    /**
     * Constructor that set values to the tree
     * @param element
     * @param value
     */
    public Node(int element, String value) {
        this.element = element;
        this.value = value;
    }

    public Node() {

    }

}
