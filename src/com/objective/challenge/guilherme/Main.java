package com.objective.challenge.guilherme;

import com.objective.challenge.guilherme.controllers.FoodController;

/**
 * @Author: Guilherme Marques
 * @Date: 2018-08-06
 */
public class Main {

    public static void main(String[] args) {
        FoodController foodController = new FoodController();

        // Verify if the tree is empty, and initialize the questions.
        if (foodController.getTree() == null)
            foodController.initializeTree();

    }

}
