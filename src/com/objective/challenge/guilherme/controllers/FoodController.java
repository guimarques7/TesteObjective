package com.objective.challenge.guilherme.controllers;

import com.objective.challenge.guilherme.interfaces.TreeInterface;
import com.objective.challenge.guilherme.utils.Node;
import javax.swing.JOptionPane;

/**
 * @Author: Guilherme Marques
 * @Date: 2018-08-06
 */
public class FoodController implements TreeInterface<Node> {

    public static int index = 1;
    public static Node tree;
    public boolean quit;
    
    /**
     * Method initializeTree, initializes a FoodTree
     */
    public void initializeTree() {
        this.setTree(new Node(index, " é massa?"));
        this.getTree().leftNode = new Node(++index, "bolo de chocolate");
        this.getTree().rightNode = new Node(++index, "lasanha");
        
        quit = false;
        // Stay in the loop until quit = false.
        do {
            JOptionPane.showMessageDialog(null, "Pense em um prato que gosta");
            
            int firstQuestion = JOptionPane.showConfirmDialog(
                    null,
                    "O prato que você pensou" + this.getTree().value,
                    "Confirme",
                    JOptionPane.YES_NO_OPTION, 
                    JOptionPane.QUESTION_MESSAGE
            );

            // Enter in a node, depending the answer.
            if (firstQuestion == 0) 
                toAsk(getTree().rightNode);
            
            if (firstQuestion == 1)
                toAsk(getTree().leftNode);
            
            // Exit application
            if (firstQuestion == JOptionPane.CLOSED_OPTION)
                quit = true;

        } while (quit == false);
        System.exit(0);
    }

    /**
     * Method getTree, implemented by the interface interface TreeInterface
     * @return Node
     */
    @Override
    public Node getTree() {
        return tree;
    }

    /**
     * Method setTree, implemented by the interface TreeInterface
     * @param node
     */
    @Override
    public void setTree(Node node) {
        tree = node;
    }

    /**
     * Method insertOption, implemented by the interface TreeInterface
     * @param node
     */
    @Override
    public void insertOption(Node node) {
        String food = getAnswer("Qual prato você pensou?");
        String diff = getAnswer("Um(a) " + food + " é ___________ mas um(a) " + node.value + " não.");
        String aux = node.value;
        
        node.value = diff;
        node.rightNode = new Node(++index, food);
        node.leftNode = new Node(++index, aux);
    }

    /**
     * Method toAsk, implemented by the interface TreeInterface
     * @param node
     */
    @Override
    public void toAsk(Node node) {
        // Do a question to verify the correct food
        int question = JOptionPane.showConfirmDialog(null, "O prato que você pensou é " + node.value + "?", "Confirme",
                JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
        
        // Exit application
        if (question == JOptionPane.CLOSED_OPTION)
            System.exit(0);

        // Answered yes
        if (question == 0) {
            // If the rightNode is not empty, do another question
            if (node.rightNode != null)
                toAsk(node.rightNode);
            
            // If the rightNode is not empty, the answer is correct
            if (node.rightNode == null)
                JOptionPane.showMessageDialog(null, "Acertei de novo!");
        }
        
        // Answered no
        if (question == 1){
            // If the leftNode is not empty, do another question
            if (node.leftNode != null)
                toAsk(node.leftNode);
            
            // If the leftNode is empty, insert a new food
            if (node.leftNode == null)
                insertOption(node);
        }
    }
    
    /**
     * Method getAnswer, returns a answer from a question
     * @param question
     * @return String
     */
    private String getAnswer(String question){
        String answer = null;
        do {
            answer = JOptionPane.showInputDialog(question);
            if(answer == null)
                System.exit(0);
        } while (answer.equals(""));

        return answer;
    }
}
