package com.objective.challenge.guilherme.interfaces;

/**
 * @Author: Guilherme Marques
 * @Date: 2018-08-06
 *
 */
public interface TreeInterface<T> {

    public void insertOption(T e);

    public void toAsk(T e);

    public T getTree();

    public void setTree(T e);

}
